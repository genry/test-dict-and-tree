#coding: utf-8
import random
import pprint

# USING PYTHON 3

def _generate_tree(nodes=30):
    """
    """
    class Node(object):
        def __init__(self):
            self.name = ''
            self.parent = None

            # for test purposes
            self.depth = 1
            self.children = []

        def __eq__(self, other):
            return self.name == other.name

        def __repr__(self):
            return self.name

        def set_name(self, counter):
            self.name = 'node%s_depth%s ->%s' % (counter, self.depth, self.parent)

    created_nodes = []
    max_depth = 1

    for counter in range(nodes):
        node = Node()

        # root node creates with default params parent = None depth = 1
        if created_nodes:
            if len(created_nodes) == 1:
                random_parent = created_nodes[0]
                random_parent.name = 'ROOT'
            else:
                random_parent = created_nodes[random.randint(0, len(created_nodes)-1)]
            node.parent = random_parent
            node.parent.children.append(node)
            node.depth = random_parent.depth + 1
            if (random_parent.depth + 1) > max_depth:
                max_depth = random_parent.depth + 1

        node.set_name(counter)
        created_nodes.append(node)

    return created_nodes


def find_common_ancestor(node_1, node_2, tree):
    def _find_same_level(node_1, node_2):
        while node_1 != node_2:
            node_1 = node_1.parent
            node_2 = node_2.parent

        return node_1

    def _find_depth(node):
        depth = 1
        while node.parent != None:
            depth += 1
            node = node.parent

        return depth

    def _move_pointer_to_depth(node, levels):
        for x in range(levels):
            node = node.parent

    depth_1 = _find_depth(node_1)
    depth_2 = _find_depth(node_2)

    if depth_1 < depth_2:
        for x in range(depth_2-depth_1):
            node_2 = node_2.parent

    elif depth_1 > depth_2:
        for x in range(depth_1-depth_2):
            node_1 = node_1.parent

    return _find_same_level(node_1, node_2)





if __name__ == '__main__':
    tree = _generate_tree()
    pprint.pprint(tree)

    node_1, node_2 = tree[random.randint(0, len(tree)-1)], tree[random.randint(0, len(tree)-1)]
    print('\n')
    print('selected two nodes: \n %s \n %s' % (node_1, node_2))
    print('\n')
    print('common ancestor: ', find_common_ancestor(node_1, node_2, tree))