#coding: utf-8
from pprint import pprint
import random


# USING PYTHON 3

def _generate_dict(length_limit=3, depth_limit=3, depth=0, counter=None):
    """

    :param length:
    :param depth:
    :return:
    """
    result = {}
    key_counter = counter or 1
    current_depth = depth or 1
    length = length_limit if not depth else random.randint(1, length_limit)
    for key in range(length):
        key_name = "key%s_depth%s" % (key_counter, current_depth)
        key_counter += 1

        random_depth = random.randint(1, depth_limit+1-current_depth)

        if random_depth == 1:
            val = random.randint(0, 10)
        else:
            val = _generate_dict(depth=current_depth+1, counter=key_counter)

        result[key_name] = val

    return result

def calculate_keys_depth(dict_, current_depth=1, result_dict=None):
    """

    :param dict_:
    :return:
    """
    output = result_dict or {}
    for key in dict_.keys():
        output[key] = current_depth
        if isinstance(dict_[key], dict):
            calculate_keys_depth(dict_[key], current_depth+1, output)

    return output


if __name__ == '__main__':
    result = _generate_dict()
    print('generated dict: ')
    pprint(result)

    print('calculated depth:')
    pprint(calculate_keys_depth(result))

